import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { List } from '../listsPage/lists'
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';

@Injectable()
export class ListsService {
    configUrl: string = 'http://127.0.0.1:4000'
    constructor(private http: HttpClient) { }

    getData(): Observable<List[]> {
        return this.http.get(this.configUrl + '/getlists')
            .pipe(
                map(data => {
                    let List = data["lists"];
                    return List.map(function (list: any) {
                        return { id: list.id, title: list.title, done_todos: list.done_todos, all_todos: list.all_todos };
                    });
                }));
    }

    postData(list: List): Observable<string> {
        return this.http.post(this.configUrl + '/postlist', list, { responseType: 'text' });
    }
}