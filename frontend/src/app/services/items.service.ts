import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Item } from '../itemPage/items'
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class ItemsService {
    configUrl: string = 'http://127.0.0.1:4000'
    constructor(private http: HttpClient) { }

    getItems(idList: number): Observable<Item[]> {
        return this.http.get(this.configUrl + '/getitems?idlist=' + idList)
            .pipe(
                map(data => {
                    let itemsList = data["items"];
                    return itemsList.map(function (item: any) {
                        return { id: item.id, id_list: item.id_list, title: item.title, done: item.done };
                    });
                }));
    }

    postItems(item: Item): Observable<any> {
        return this.http.post(this.configUrl + '/postitem', item, { responseType: 'text' })
    }
    putItem(item: Item): Observable<any> {
        return this.http.put(this.configUrl + '/putitem', item, { responseType: 'text' })
    }
}