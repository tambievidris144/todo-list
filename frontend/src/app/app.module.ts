import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { ListComponent } from './list/list.component'
import { ItemComponent } from './items/item.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations'
import { ItemsPageComponent } from './itemPage/items-page.component'
import { ListsPageComponent } from './listsPage/lists-page.component'
import { from } from 'rxjs';
import { MatRadioModule } from '@angular/material/radio';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { HttpClientModule } from '@angular/common/http';

const appRoutes: Routes = [
  { path: '', component: ListsPageComponent },
  { path: 'list/:id', component: ItemsPageComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    ListComponent,
    ItemComponent,
    ListsPageComponent,
    ItemsPageComponent
  ],
  imports: [
    BrowserModule,
    NoopAnimationsModule,
    RouterModule.forRoot(appRoutes),
    MatRadioModule,
    HttpClientModule,
    MatButtonModule,
    MatCardModule,
    MatInputModule,
    MatIconModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
