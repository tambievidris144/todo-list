import { Input, Component } from '@angular/core'
import { List } from '../listsPage/lists'

@Component({
    selector: 'app-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.less']
})

export class ListComponent {
    @Input() lists: List
}