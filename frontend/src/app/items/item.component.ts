import { Input, Component, Output, EventEmitter } from '@angular/core'
import { Item } from '../itemPage/items'
import { from } from 'rxjs'

@Component({
    selector: 'app-item',
    templateUrl: './item.component.html',
    styleUrls: ['./item.component.less']
})
export class ItemComponent {

    @Input() items: Item
    @Output() newItemEvent = new EventEmitter<number>();

    addNewItem(value: number) {
        this.newItemEvent.emit(value);
    }
}