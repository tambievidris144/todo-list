import { Input, Component, OnInit } from '@angular/core'
import { ListsService } from '../services/lists.service';
import { List } from './lists'

@Component({
    selector: 'app-lists-page',
    templateUrl: './lists-page.component.html',
    styleUrls: ['./lists-page.component.less'],
    providers: [ListsService]
})

export class ListsPageComponent implements OnInit {
    lists: List[] = [];
    newList: List = new List();
    newTitle: string;
    constructor(private httpService: ListsService) { }

    ngOnInit() {
        this.getAllLists()
    }

    getAllLists() {
        this.httpService
            .getData()
            .subscribe(
                data => (
                    this.lists = data
                )
            );
    }

    inputChanged(event) {
        this.newTitle = event.target.value;
    }

    CreateNewList() {
        if (this.newTitle !== undefined && this.newTitle !== '') {
            this.newList.id = this.lists.length + 1
            this.newList.title = this.newTitle
            this.submitNewList(this.newList)
        }
    }

    submitNewList(list: List) {
        this.httpService.postData(list)
            .subscribe(
                (response: string) => { this.getAllLists() },
                (error) => console.log(error)
            );

    }
}