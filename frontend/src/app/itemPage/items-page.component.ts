import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ItemsService } from '../services/items.service'
import { Item } from './items';

@Component({
    selector: 'app-item-page',
    templateUrl: './items-page.component.html',
    styleUrls: ['./items-page.component.less'],
    providers: [ItemsService]
})

export class ItemsPageComponent implements OnInit {
    items: Item[] = []
    filteredItems: Item[] = []
    newUpdateItem: Item = new Item()
    idFilter: number
    newItem: Item = new Item()
    filterItemStatus: boolean = true


    constructor(
        private route: ActivatedRoute,
        private httpService: ItemsService
    ) { }

    ngOnInit() {
        const routeParams = this.route.snapshot.paramMap;
        this.idFilter = Number(routeParams.get('id'))
        console.log('ngOnInit')
        this.getItems(true)
    }

    getItems(status: boolean) {
        this.httpService.getItems(this.idFilter).subscribe(data => {
            this.items = data;
            this.filterItemStatus = status
            if (status === false) {
                this.filteredItems = this.items.filter(function (item) {
                    return item.done == status;
                })
            } else {
                this.filteredItems = this.items
            }
        })
    }

    createNewItem(event: any) {
        const value = event.target.value
        if (value !== '') {
            this.newItem.id = this.items.length
            this.newItem.id_list = this.idFilter
            this.newItem.title = value
            this.newItem.done = false
            this.submitNewItem(this.newItem)
            event.target.value = ``
        }
    }

    searchItem(event: any) {
        const value = event.target.value
        if (value !== '') {
            this.filteredItems = this.items.filter(function (item) {
                return item.title.toUpperCase().includes(value.toUpperCase());
            })
        } else {
            this.getItems(true)
        }
    }

    submitNewItem(item: Item) {
        this.httpService.postItems(item)
            .subscribe(
                (response: any) => {
                    this.getItems(this.filterItemStatus)
                },
                error => console.log(error)
            );
        console.log('submit')
    }

    updateItemDone(newItem: number) {
        this.newUpdateItem.id_list = this.idFilter
        this.newUpdateItem.id = newItem
        let itemDone: boolean
        this.items.forEach(function (item, i, arr) {
            if (item.id === newItem) {
                itemDone = !item.done
            }
        });
        this.newUpdateItem.done = itemDone
        this.httpService.putItem(this.newUpdateItem)
            .subscribe(
                (response: any) => {
                    console.log(response)
                    this.getItems(this.filterItemStatus)
                },
                error => console.log(error)
            );
    }
}