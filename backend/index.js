const express = require('express')
const mysql = require("mysql");
const cors = require('cors');
const async = require('async');
const PORT = 4000
const app = express()
const jsonParser = express.json();
app.use(cors());

const connection = mysql.createConnection({
    host: "localhost",
    user: "root",
    database: "todo_list",
    password: ""
});

app.use(function (req, res, next) {
    var origins = [
        '127.0.0.1:4000',
        'http://127.0.0.1:4000'
    ];
    for (var i = 0; i < origins.length; i++) {
        var origin = origins[i];
        if (req.headers.origin.indexOf(origin) > -1) {
            res.header('Access-Control-Allow-Origin', req.headers.origin);
        }
    }
    res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

//Get all lists
app.get("/getlists", function (req, res) {
    connection.query("SELECT * FROM lists", function (err, data) {
        if (err) return console.log(err);
        res.json({ "lists": data })
    });
})

//Insert new list
app.post("/postlist", jsonParser, async function (request, response) {
    const insertSql = `INSERT INTO lists (title, done_todos, all_todos) VALUES(?,?,?)`;
    if (!request.body) return response.sendStatus(400);
    let list = [
        request.body.title,
        0,
        0
    ]
    connection.connect(function (err) {
        connection.query(insertSql, list, function (err) {
            console.log(list)
            if (err) return console.log(err);
        });
    })
    response.send("Insert new list was successful")
});

//Get all items
app.get('/getitems', function (request, response) {
    connection.query(`SELECT * FROM todos WHERE id_list=${parseInt(request.query.idlist)}`, function (err, data) {
        if (err) return console.log(err);
        response.json({ "items": data })
    });

})

//Post insert new todo
app.post("/postitem", jsonParser, function (request, response) {
    if (!request.body) return response.sendStatus(400);
    const item = [
        request.body.id_list,
        request.body.title,
        request.body.done
    ]
    const sql = "INSERT INTO todos (id_list, title, done) VALUES(?,?,?)";
    connection.connect(async function (err) {
        async.series([
            function (callback) {
                connection.query(sql, item, async function (err) {
                    if (err) return console.log(err);
                })
                callback();
            },
            function (callback) {
                updateList(item[0])
            }
        ],
            function (error, results) {
            }
        )
    })
    response.send("Insert new item was successful")

});

//Update item
app.put('/putitem', jsonParser, function (request, response) {
    if (!request.body) return response.sendStatus(400);
    const updateCountSql = `UPDATE todos SET done=${request.body.done} WHERE id=${request.body.id}`
    connection.query(updateCountSql, function (err) {
        if (err) return console.log(err);
        console.log('update item')
        updateList(request.body.id_list)
        response.send("Update item was successful")
    })
})

function updateList(id) {
    let done = []
    let updateCountSql = ``
    const selectAllSql = `SELECT COUNT(*) as count FROM todos WHERE id_list=${id}`
    const selectDoneSql = `SELECT COUNT(*) as count FROM todos WHERE id_list=${id} AND done=true`
    async.series([
        function (callback) {
            connection.query(selectDoneSql, function (err, data) {
                if (err) return console.log(err);
                done[0] = data[0].count
            })
            callback();
        },
        function (callback) {
            connection.query(selectAllSql, function (err, data) {
                if (err) return console.log(err);
                done[1] = data[0].count
                updateCountSql = `UPDATE lists SET done_todos=${done[0]}, all_todos=${done[1]} WHERE id=${id}`
                connection.query(updateCountSql, function (err) {
                    if (err) return console.log(err);
                })
            })
            callback();
        }
    ],
        function (error, results) {
        }
    )
}
app.listen(PORT)